
// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---cache-dev-404-page-js": preferDefault(require("/Users/pavlokuzina/projects/test-gatsby-repo/gatsby/examples/client-only-paths/.cache/dev-404-page.js")),
  "component---src-pages-js": preferDefault(require("/Users/pavlokuzina/projects/test-gatsby-repo/gatsby/examples/client-only-paths/src/pages/[...].js"))
}

